import service from "@/api/request";
import {Sessionset} from "@/api/session";


export const hisuser = async(data) => {
    let res= await service({
        url: '/user/hisuser',
        method: 'get',
        params: data
    })
    return res.data
}


export const accresslist = async(data) => {
    let res= await service({
        url: '/user/accress',
        method: 'get',
        params: data
    })
    return res.data
}


export const userlist = async(data) => {
    let res= await service({
        url: '/user/userlist',
        method: 'get',
        params: data
    })
    return res.data
}

export const userdel = async(data) => {
    let res= await service({
        url: '/user/userdel',
        method: 'get',
        params: data
    })
    return res.data
}

export const resetpass = async(data) => {
    let res= await service({
        url: '/user/resetpass',
        method: 'get',
        params: data
    })
    return res.data
}

export const useradd = async(data) => {
    let res = await service({
        url: '/user/useradd',
        method: 'post',
        data: data
    })
    return res.data
}

export const changepas = async(data) => {
    let res = await service({
        url: '/user/changepass',
        method: 'post',
        data: data
    })
    return res.data
}
