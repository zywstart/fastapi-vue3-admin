import service from '@/api/request'
import Cookies from 'js-cookie';

export const Session = {
    // 设置临时缓存
    Sessionset(key, val) {
        if (key === 'token') return Cookies.set(key, val);
        console.log(key)
        console.log(val)
        console.log(JSON.stringify(val))
        window.sessionStorage.setItem(key,JSON.stringify(val));
    },
    // 获取临时缓存
    get(key) {
        if (key === 'token') return Cookies.get(key);
        let json = window.sessionStorage.getItem(key);
        return JSON.parse(json);
    },
    // 移除临时缓存
    remove(key) {
        if (key === 'token') return Cookies.remove(key);
        window.sessionStorage.removeItem(Local.setKey(key));
    },
    // 移除全部临时缓存
    clear() {
        Cookies.remove('token');
        window.sessionStorage.clear();
    },
};


export const Sessionset=(key, val)=> {
    window.sessionStorage.setItem(key,JSON.stringify(val));
    if (key === 'token') return Cookies.set(key, val);
}


export const login = async(data) => {
    let res= await service({
        url: '/user/login',
        method: 'post',
        data: data
    })
    Sessionset('token',res.data.token)
    Sessionset('user',res.data.user)
    return res.data
}