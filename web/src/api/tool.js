

export const formatDate=(row, column)=>{
    // 获取单元格数据
    let data = row[column.property];
    if(data == null||data=='0001-01-01T00:00:00Z') {
        return null;
    }
    let dt = new Date(data)
    return dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() + ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds()
}