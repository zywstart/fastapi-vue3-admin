# -*- coding: utf-8 -*-
"""
@Time ： 2021/12/29 16:18
@Auth ： Jolg
@File ：log.py
@IDE ：PyCharm

"""
from loguru import logger
from apps.base.conf import BASE_DIR,conf,LogLevel
import os,sys

log_path = os.path.join(BASE_DIR, 'logs')
log_file = os.path.join(log_path, f'Run.log')
logger.remove()
logger.add(sys.stderr, level=LogLevel)
logger.add(log_file, level=LogLevel)
logger.configure(handlers=[
    {
        "sink": sys.stderr,
        "format": "{time:YYYY-MM-DD HH:mm:ss.SSS} |<lvl>{level:8}</>| {name}{line:4} - <lvl>{message}</>",
        "colorize": True
    },
])

def initlog():
    if not os.path.exists(log_path):
        os.mkdir(log_path)

