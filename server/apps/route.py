# !/usr/bin/env Python3
# -*- coding: utf-8 -*-
# 作者   : 王宗龙
# 文件     : route
# 时间     : 2020/12/9 11:45
# 开发工具 : PyCharm


from fastapi import FastAPI,responses
from starlette.staticfiles import StaticFiles

from apps.base import user
from apps.base.base import cors,Access
from apps.base.conf import conf


def createapp():
    app = FastAPI(
        title=conf.title,
        description=conf.description,
        version=conf.VERSION,
    )
    # app.mount('/static', StaticFiles(directory='apps/static'), name='static')
    app.mount('/web',StaticFiles(directory='static'), name='web')
    app.mount('/assets', StaticFiles(directory='static/assets'), name='web')
    #用户相关
    app.include_router(user.route, prefix='/user', tags=["用户"])
    @app.get("/")
    def home():
        return responses.RedirectResponse(url="/web/index.html")

    cors(app) # 跨域设置
    Access(app)
    return app
