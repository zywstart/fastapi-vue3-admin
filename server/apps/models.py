# -*- coding: utf-8 -*-
"""
@Time ： 2021/12/30 13:21
@Auth ： Jolg
@File ：models.py
@IDE ：PyCharm

"""
from sqlalchemy import Column, Integer, String,DateTime, ForeignKey,Boolean
from sqlalchemy.orm import declarative_base
from apps.base.db import engine,Newsession
import time,datetime
from sqlalchemy.sql import func
from apps.base.log import logger
Base = declarative_base()

class Basejson():
    def json(self):
        dict = self.__dict__
        if "_sa_instance_state" in dict:
            del dict["_sa_instance_state"]
        return dict

class Users(Base,Basejson):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String(64), unique=True)
    passwd = Column(String(64),default='e10adc3949ba59abbe56e057f20f883e')
    nicename = Column(String(64))
    role = Column(String(64))
    status = Column(Boolean,default=True)
    create= Column(DateTime(), default=datetime.datetime.now())
    lastlogin = Column(DateTime())
    def __init__(self, name,nicename,passwd,role,status):
        self.name = name
        self.nicename=nicename
        self.passwd=passwd
        self.role = role
        self.status=status
    # def __str__(self):
    #     return self.name

class AccessRecords(Base):
    __tablename__ = "accessrecords"
    id = Column(Integer, primary_key=True)
    userid = Column(Integer)
    username = Column(String(64))
    nicename = Column(String(164))
    fromip = Column(String(164))
    url = Column(String(640))
    method = Column(String(64))
    access_date =Column(DateTime(), default=datetime.datetime.now())

def inituser():
    session=Newsession()
    user=session.query(Users).filter_by(name='admin').first()
    if not user:
        adminuser=Users(name='admin', nicename="管理员",role='admin', passwd='e10adc3949ba59abbe56e057f20f883e',status=True)
        session.add(adminuser)
        session.commit();
        session.flush()


class Loginhistory(Base):
    __tablename__ = "loginhistory"
    id = Column(Integer, primary_key=True)
    username = Column(String(64))
    nicename = Column(String(164))
    fromip = Column(String(164))
    resout = Column(Boolean, default=True)
    login_date =Column(DateTime(), default=datetime.datetime.now())

def initdb():
    Base.metadata.create_all(engine)
    inituser()

